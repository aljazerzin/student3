# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija
Naloga 6.2.1:
https://bitbucket.org/Mberga14/stroboskop
Naloga 6.2.2:

git clone https://Mberga14@bitbucket.org/Mberga14/stroboskop.git
git remote add master  https://Mberga14@bitbucket.org/Mberga14/stroboskop.git
Naloga 6.2.3:
git remove nepotrebno.js
git add -A
git commit -a -m "Priprava potrebnih JavaScript knjižnic"
git push origin master
https://bitbucket.org/Mberga14/stroboskop/commits/a2b6f7ae08f880ddb4649902ef424f3ae8e16bb3



## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
git branch izgled
git checkout izgled
git add -A
git commit -a -m "Dodajanje stilov spletne strani"
git push origin izgled
https://bitbucket.org/Mberga14/stroboskop/commits/1372bae4db6e698ca76b3bbb8138f66352214d7a
Naloga 6.3.2:
git add -A
git commit -a -m "Desna poravnava besedil"
git push origin izgled
https://bitbucket.org/Mberga14/stroboskop/commits/1ccc7a16caafa8e3be9495adc736458b4dd1e4a9
Naloga 6.3.3:
git add -A
git commit -a -m "Dodajanje gumba za dodajanje barv"
git push origin izgled
https://bitbucket.org/Mberga14/stroboskop/commits/8f0bc874236c22c36674531a40f15ce3f2bbbb76
Naloga 6.3.4:
git add -A
git commit -a -m "dodajanje robov in senc"
git push origin izgled
https://bitbucket.org/Mberga14/stroboskop/commits/72e59ffcebcc74cae458455ce945e322a07a47f8
Naloga 6.3.5:
git checkout master
git merge izgled
git add -A
git push origin master

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
git checkout -b dinamika
git add -A
git commit -a -m "Dodajanje izbiranja barv"
git push origin dinamika
https://bitbucket.org/Mberga14/stroboskop/commits/f1d3aedc184cd33d97a1be43c8174f7dda816e53
Naloga 6.4.2:
git add -A
git commit -a -m "Dinamika gumba za dodajanje barv"
git push origin dinamika
https://bitbucket.org/Mberga14/stroboskop/commits/6fc07a28f64e2986b6b99921503914be40a7531f
Naloga 6.4.3:
https://bitbucket.org/Mberga14/stroboskop/commits/031ef57d7e6824aefe9a627ed840146521be2255

Naloga 6.4.4:
https://bitbucket.org/Mberga14/stroboskop/commits/ecbb9d485a30e9a5031aea68e342009029878cb7